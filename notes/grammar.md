# Grammar Overview & Evolution

## First Pass of Grammar
Start Date: 9/22/2019

expression     → addition ;

addition       → multiplication ( ( "-" | "+" ) multiplication )* ;

multiplication → primary ( ( "/" | "*" ) primary )* ;

primary        → NUMBER | "(" expression ")" ;

Notes:
I need to proof this to make sure that it is valid and complete from a grammar standpoint.

## Second Pass of Grammar
