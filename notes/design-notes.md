# Design Notes

## First Pass of Design - v0.0.1
Start Date: 9/22/2019


### Basic Math functionality
First pass of this language will be a glorified calculator. This should handle the implementation of something like this:
    
    (1 + 2) * 3 + 2 
    
      => 11

    1 + 2 * 3 + 2 
    
      => 9

    1 / 2
    
      => 0.5

This should ignore white space, as well as allow for multiline so the following three are the same:


    ( 1 * ( 3 + 2 ) / 5 )


    (1*(3+2)/5)


    ( 1 * 

      ( 3 + 2 ) 

    / 5 )

There will be a basic repl to evaluate these expressions, and it will also evaluate expressions similar to this in
in in files with the extension .cio

### Errors
The only errors that can occur is when an invalid character is provided, or there are not complete pairs of parens '()'

## v0.0.2 - Variables, Mutability Check, Printing, and Comments
I would like to extend out the language to include statements & declarations - just expressions is pretty limited.  
For this 0.0.2 version of Curio I want to play with some different concepts so I am going to work on getting the machinary in place for statments, but not add too many.

### Variables & Assignment

    let my_var = "Hello world!";
    let mut my_mutable_var = 123;
    my_mutable_var = 1;


I will need to add IDENTIFIER and STRING to the primary grammar.  Under the hood I think this will take a form similar to Rust, where the variable is immutable by default.  If the mut keyword is present, like in the second example, then it can be updated freely.  I am not sure how to implement this concept of immutability in the compiler though, since JS doesn't have a true concept of immutability (no, const doesn't count in this context - but I will still use it under the hood in code generation).

Items to research / implement to get this done:

- [ ] Add Stmt / declaration handling to the language.
- [ ] Figure out how to do a mutability pass on the AST before generating code.
  - [ ] Research: Is just the AST ok here?  Do I need to start understanding Intermediate Representation (IR) concepts?
  - [ ] Actually implement this check, and report errors when trying to modify immutable variables.

### Printing


    print my_var;


Next I want to add printing - this will take the form of a keyword for now.  I am not entirely sure how to start creating a standard library / what is going to be needed for a standard library.
I am just adding this now so that way we can generate some output that does *something* in JS at this point.

An idea I want to play with is the idea of adding some sort of annotation before blocks of code, maybe specific to things like printing, that will allow the generated code to easily exclude things in "prod" builds that we don't want - such as debug info!


    @dev_only
    print my_debug_code;


### Comments
Addition of comments in source code, these will be ignored by the parser at this time.  I like the idea of exploring how
comments can be used to do doctests, and building a compiler tool that generates documentation nativly (like elixir).

### Example Code Snippits

Will work:

    let my_string = "Hello World!";
    print my_string;


    let mut my_num = 5;
    my_num = my_num + 6;

    print my_num; // Should print 11

Will not compile:


    let my_num = 5;
    my_num = 6;
