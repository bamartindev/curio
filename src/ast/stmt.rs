use super::expr::Expr;

#[derive(Debug, PartialEq)]
pub enum Stmt {
    Expression(Expr),
    Print(Expr),
    Let(String, Option<Expr>),
}

pub trait Visitor<T> {
    fn visit_expression_stmt(&mut self, expr: &Expr) -> T;
    fn visit_print(&mut self, expr: &Expr) -> T;
    fn visit_let(&mut self, name: &str, expr: &Option<Expr>) -> T;
}

impl Stmt {
    pub fn accept<T>(&self, v: &mut dyn Visitor<T>) -> T {
        match *self {
            Stmt::Expression(ref expr) => v.visit_expression_stmt(expr),
            Stmt::Print(ref expr) => v.visit_print(expr),
            Stmt::Let(ref name, ref expr) => v.visit_let(name, expr),
        }
    }
}
