use std::fmt;

#[derive(Debug, PartialEq, Clone)]
pub struct Token {
    typ: TokenType,
    lexeme: String,
    line: usize,
}

// Associated methods
impl Token {
    pub fn new(typ: TokenType, lexeme: String, line: usize) -> Self {
        Self { typ, lexeme, line }
    }
}

// Member methods
impl Token {
    pub fn typ(&self) -> &TokenType {
        &self.typ
    }

    pub fn lexeme(&self) -> &String {
        &self.lexeme
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {} {}", self.typ, self.lexeme, self.line)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum TokenType {
    Plus,
    Minus,
    Star,
    Slash,
    LeftParen,
    RightParen,
    Equal,
    Semicolon,
    Number(f64),
    String(String),
    Identifier,
    Print,
    Let,
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use TokenType::*;

        match self {
            Plus => write!(f, "Plus"),
            Minus => write!(f, "Minus"),
            Star => write!(f, "Star"),
            Slash => write!(f, "Slash"),
            LeftParen => write!(f, "LeftParen"),
            RightParen => write!(f, "RightParen"),
            Equal => write!(f, "Equal"),
            Semicolon => write!(f, "Semicolon"),
            Number(value) => write!(f, "Number: {}", value),
            String(value) => write!(f, "String: {}", value),
            Identifier => write!(f, "Identifier"),
            Print => write!(f, "Print"),
            Let => write!(f, "Let"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Token, TokenType};

    #[test]
    fn token_new() {
        let expected = Token {
            typ: TokenType::LeftParen,
            lexeme: String::from("("),
            line: 0,
        };

        assert_eq!(
            expected,
            Token::new(TokenType::LeftParen, String::from("("), 0)
        );
    }

    #[test]
    fn token_get_typ() {
        let token = Token {
            typ: TokenType::Minus,
            lexeme: String::from("-"),
            line: 0,
        };
        assert_eq!(&TokenType::Minus, token.typ())
    }

    #[test]
    fn token_get_lexeme() {
        let token = Token {
            typ: TokenType::Minus,
            lexeme: String::from("-"),
            line: 0,
        };
        assert_eq!("-", token.lexeme())
    }
}
