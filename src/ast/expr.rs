use super::token::Token;

#[derive(Debug, PartialEq, Clone)]
pub enum Expr {
    Literal(Token),
    Binary(Box<Expr>, Token, Box<Expr>),
    Grouping(Box<Expr>),
}

// Visitor trait for visitor pattern: https://en.wikipedia.org/wiki/Visitor_pattern
pub trait Visitor<T> {
    fn visit_literal(&mut self, lit: &Token) -> T;
    fn visit_binary(&mut self, lhs: &Expr, op: &Token, rhs: &Expr) -> T;
    fn visit_grouping(&mut self, inside: &Expr) -> T;
}

impl Expr {
    pub fn accept<T>(&self, v: &mut dyn Visitor<T>) -> T {
        match *self {
            Expr::Literal(ref lit) => v.visit_literal(lit),
            Expr::Binary(ref lhs, ref op, ref rhs) => v.visit_binary(lhs, op, rhs),
            Expr::Grouping(ref inside) => v.visit_grouping(inside),
        }
    }
}
