// Trying to write a visitor that takes the AST and generates basic JS code.
use crate::ast::expr::{Expr, Visitor as ExprVisitor};
use crate::ast::stmt::{Stmt, Visitor as StmtVisitor};
use crate::ast::token::{Token, TokenType};

pub struct Print {}

// Associated methods
impl Print {
    pub fn new() -> Self {
        Self {}
    }
}

impl Print {
    pub fn generate_string(&mut self, statements: &[Stmt]) -> String {
        let mut result = String::new();
        for stmt in statements {
            result.push_str(&stmt.accept(self));
        }
        result
    }

    fn evaluate(&mut self, expr: &Expr) -> String {
        expr.accept(self)
    }
}

impl StmtVisitor<String> for Print {
    fn visit_expression_stmt(&mut self, expr: &Expr) -> String {
        self.evaluate(expr)
    }

    fn visit_print(&mut self, expr: &Expr) -> String {
        let expr = self.evaluate(expr);
        format!("console.log({});", expr)
    }

    fn visit_let(&mut self, name: &str, expr: &Option<Expr>) -> String {
        match expr {
            Some(ref expr) => {
                let expr = self.evaluate(expr);
                format!("const {} = {};", name, expr)
            }
            None => format!("let {};", name),
        }
    }
}

impl ExprVisitor<String> for Print {
    fn visit_literal(&mut self, lit: &Token) -> String {
        match lit.typ() {
            TokenType::Number(num) => num.to_string(),
            TokenType::String(string) => format!("\"{}\"", string.clone()),
            TokenType::Identifier => lit.lexeme().to_owned(),
            _ => panic!("Unexpected literal type found in visit_literal"),
        }
    }

    fn visit_binary(&mut self, lhs: &Expr, op: &Token, rhs: &Expr) -> String {
        let lhs = self.evaluate(lhs);
        let rhs = self.evaluate(rhs);

        format!("{} {} {}", lhs, op.lexeme(), rhs)
    }

    fn visit_grouping(&mut self, inside: &Expr) -> String {
        let inside = self.evaluate(inside);

        format!("({})", inside)
    }
}
