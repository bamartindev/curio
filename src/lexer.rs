use std::collections::HashMap;
use std::iter::Peekable;
use std::str::Chars;

use crate::ast::token::{Token, TokenType};

lazy_static! {
    static ref KEYWORDS: HashMap<String, TokenType> = {
        let mut m = HashMap::new();
        m.insert("print".to_owned(), TokenType::Print);
        m.insert("let".to_owned(), TokenType::Let);
        m
    };
}

// The purpose of a lexer is to take a source string as input
// and return a collection (vector) of Tokens.
pub struct Lexer<'a> {
    source: Peekable<Chars<'a>>,
    line: usize,
}

// Associated methods
impl<'a> Lexer<'a> {
    pub fn new(source: &'a str) -> Self {
        Self {
            source: source.chars().peekable(),
            line: 1,
        }
    }

    fn is_legal_identifier_char(ch: char) -> bool {
        match ch {
            'A'..='Z' | 'a'..='z' | '_' | '0'..='9' => true,
            _ => false,
        }
    }
}

// Member Methods
impl<'a> Lexer<'a> {
    pub fn lex(&mut self) -> Vec<Token> {
        let mut tokens = Vec::new();

        while let Some(ch) = self.source.next() {
            let token = match ch {
                '+' => Some(Token::new(TokenType::Plus, ch.to_string(), self.line)),
                '-' => Some(Token::new(TokenType::Minus, ch.to_string(), self.line)),
                '*' => Some(Token::new(TokenType::Star, ch.to_string(), self.line)),
                '/' => Some(Token::new(TokenType::Slash, ch.to_string(), self.line)),
                '(' => Some(Token::new(TokenType::LeftParen, ch.to_string(), self.line)),
                ')' => Some(Token::new(TokenType::RightParen, ch.to_string(), self.line)),
                '=' => Some(Token::new(TokenType::Equal, ch.to_string(), self.line)),
                ';' => Some(Token::new(TokenType::Semicolon, ch.to_string(), self.line)),
                '"' => Some(self.build_string()),
                '0'..='9' => Some(self.build_num(ch)),
                'A'..='Z' | 'a'..='z' | '_' => Some(self.build_identifier(ch)),
                ' ' | '\r' | '\t' => None, // Just consume the space
                '\n' => {
                    self.line += 1;
                    None
                }
                _ => panic!("Unidentified token!"), // TODO: Create reporting context to write this to.
            };

            if let Some(token) = token {
                tokens.push(token);
            }
        }

        tokens
    }

    // TODO: This is limited to not allow escaped quotes like \" in the string.
    // Also no multiline strings.
    fn build_string(&mut self) -> Token {
        let mut string = String::new();
        let mut lexeme = None;

        while let Some(&c) = self.source.peek() {
            match c {
                '"' => {
                    lexeme = Some(format!("\"{}\"", string));
                    self.source.next();
                    break;
                }
                '\n' => break,
                _ => {
                    string.push(c);
                    self.source.next();
                }
            }
        }

        match lexeme {
            Some(lexeme) => Token::new(TokenType::String(string), lexeme, self.line),
            None => panic!("Expected closing \""), // TODO: Add error reporting context
        }
    }

    fn build_num(&mut self, first_digit: char) -> Token {
        let mut num = String::new();
        num.push(first_digit);

        while let Some(&c) = self.source.peek() {
            if c == '.' || c.is_digit(10) {
                num.push(c);
                self.source.next();
            } else {
                break;
            }
        }

        let num: f64 = num.parse().unwrap();

        Token::new(TokenType::Number(num), num.to_string(), self.line)
    }

    fn build_identifier(&mut self, ch: char) -> Token {
        let mut id = String::new();
        id.push(ch);

        while let Some(&c) = self.source.peek() {
            if Lexer::is_legal_identifier_char(c) {
                id.push(c);
                self.source.next();
            } else {
                // Gross, lets fix this.
                break;
            }
        }

        let typ = match KEYWORDS.get(&id) {
            Some(token) => token.clone(),
            None => TokenType::Identifier,
        };

        Token::new(typ, id, self.line)
    }
}

#[cfg(test)]
mod tests {
    use super::Lexer;
    use crate::ast::token::{Token, TokenType};

    #[test]
    fn simple_chars() {
        let line = 1;
        let expected = vec![
            Token::new(TokenType::LeftParen, String::from("("), line),
            Token::new(TokenType::RightParen, String::from(")"), line),
            Token::new(TokenType::Plus, String::from("+"), line),
            Token::new(TokenType::Minus, String::from("-"), line),
            Token::new(TokenType::Slash, String::from("/"), line),
            Token::new(TokenType::Star, String::from("*"), line),
        ];

        let mut lexer = Lexer::new("()+-/*");
        assert_eq!(expected, lexer.lex());
    }

    #[test]
    fn floating_number() {
        let line = 1;
        let expected = vec![
            Token::new(TokenType::Number(12.34), String::from("12.34"), line),
            Token::new(TokenType::Plus, String::from("+"), line),
            Token::new(TokenType::Number(56.789), String::from("56.789"), line),
        ];

        let mut lexer = Lexer::new("12.34 + 56.789");
        assert_eq!(expected, lexer.lex());
    }

    #[test]
    fn floating_number_from_int() {
        let line = 1;
        let expected = vec![
            Token::new(TokenType::Number(1.0), String::from("1"), line),
            Token::new(TokenType::Plus, String::from("+"), line),
            Token::new(TokenType::Number(2.0), String::from("2"), line),
        ];

        let mut lexer = Lexer::new("1 + 2");
        assert_eq!(expected, lexer.lex());
    }

    #[test]
    fn spaces_are_ignored() {
        let mut lexer_with_space = Lexer::new("( 1 * ( 3 + 2 ) / 5 )");
        let mut lexer_without_space = Lexer::new("(1*(3+2)/5)");

        assert_eq!(lexer_with_space.lex(), lexer_without_space.lex())
    }

    #[test]
    fn keyword_works() {
        let line = 1;
        let expected = vec![
            Token::new(TokenType::Print, String::from("print"), line),
            Token::new(TokenType::Number(1.0), String::from("1"), line),
            Token::new(TokenType::Plus, String::from("+"), line),
            Token::new(TokenType::Number(2.0), String::from("2"), line),
            Token::new(TokenType::Semicolon, String::from(";"), line),
        ];

        let mut lexer = Lexer::new("print 1 + 2;");
        assert_eq!(expected, lexer.lex());
    }

    #[test]
    fn variable_decl() {
        let line = 1;
        let expected = vec![
            Token::new(TokenType::Let, String::from("let"), line),
            Token::new(TokenType::Identifier, String::from("my_var"), line),
            Token::new(TokenType::Equal, String::from("="), line),
            Token::new(TokenType::Number(42.0), String::from("42"), line),
            Token::new(TokenType::Semicolon, String::from(";"), line),
        ];

        let mut lexer = Lexer::new("let my_var = 42;");
        assert_eq!(expected, lexer.lex());
    }
}
