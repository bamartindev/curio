// This parser is implemented using recursive decent parsing.  I think it might be interesting
// to learn about other types of parsing and see if I can swap them out to see what
// works the best.

use crate::ast::expr::Expr;
use crate::ast::stmt::Stmt;
use crate::ast::token::{Token, TokenType};

use std::iter::Peekable;

type PeekableTokens<'a> = Peekable<std::slice::Iter<'a, Token>>;

pub struct Parser<'a> {
    tokens: PeekableTokens<'a>,
}

// Associated methods
impl<'a> Parser<'a> {
    pub fn new(tokens: &'a [Token]) -> Self {
        Self {
            tokens: tokens.iter().peekable(),
        }
    }

    fn declaration(tokens: &mut PeekableTokens<'a>) -> Stmt {
        if Parser::is_match(tokens, &[TokenType::Let]).is_some() {
            return Parser::let_declaration(tokens);
        }

        Parser::statement(tokens)
    }

    fn statement(tokens: &mut PeekableTokens<'a>) -> Stmt {
        if Parser::is_match(tokens, &[TokenType::Print]).is_some() {
            return Parser::print_statement(tokens);
        }

        Parser::expression_statement(tokens)
    }

    fn let_declaration(tokens: &mut PeekableTokens<'a>) -> Stmt {
        let name =
            Parser::consume_or_panic(tokens, TokenType::Identifier, "Expected variable name.")
                .lexeme();
        let initializer = if Parser::is_match(tokens, &[TokenType::Equal]).is_some() {
            Some(Parser::expression(tokens))
        } else {
            None
        };

        Parser::consume_or_panic(tokens, TokenType::Semicolon, "Expected ';' after value");
        Stmt::Let(name.to_owned(), initializer)
    }

    fn print_statement(tokens: &mut PeekableTokens<'a>) -> Stmt {
        let value = Parser::expression(tokens);
        Parser::consume_or_panic(tokens, TokenType::Semicolon, "Expected ';' after value");
        Stmt::Print(value)
    }

    fn expression_statement(tokens: &mut PeekableTokens<'a>) -> Stmt {
        let expr = Parser::expression(tokens);
        Parser::consume_or_panic(tokens, TokenType::Semicolon, "Expected ';' after value");
        Stmt::Expression(expr)
    }

    fn expression(tokens: &mut PeekableTokens<'a>) -> Expr {
        Parser::addition(tokens)
    }

    fn addition(tokens: &mut PeekableTokens<'a>) -> Expr {
        let mut expr = Parser::multiplication(tokens);

        while let Some(token) = Parser::is_match(tokens, &[TokenType::Plus, TokenType::Minus]) {
            expr = Expr::Binary(
                Box::new(expr),
                token.clone(),
                Box::new(Parser::multiplication(tokens)),
            );
        }

        expr
    }

    fn multiplication(tokens: &mut PeekableTokens<'a>) -> Expr {
        let mut expr = Parser::primary(tokens);

        while let Some(token) = Parser::is_match(tokens, &[TokenType::Star, TokenType::Slash]) {
            expr = Expr::Binary(
                Box::new(expr),
                token.clone(),
                Box::new(Parser::primary(tokens)),
            );
        }

        expr
    }

    fn primary(tokens: &mut PeekableTokens<'a>) -> Expr {
        // I am not a huge fan of this, going to peek and see if I can match to the type
        if let Some(token) = tokens.peek() {
            match token.typ() {
                TokenType::Number(_) | TokenType::String(_) | TokenType::Identifier => {
                    return Expr::Literal(tokens.next().unwrap().clone())
                }
                _ => (),
            }
        }

        if Parser::is_match(tokens, &[TokenType::LeftParen]).is_some() {
            let expr = Parser::expression(tokens);
            Parser::consume_or_panic(tokens, TokenType::RightParen, "Expected closing ')'");
            return Expr::Grouping(Box::new(expr));
        }

        unreachable!()
    }

    fn is_match(tokens: &mut PeekableTokens<'a>, types: &[TokenType]) -> Option<&'a Token> {
        if let Some(token) = tokens.peek() {
            if types.contains(&token.typ()) {
                return tokens.next();
            }
        }

        None
    }

    fn consume_or_panic(
        tokens: &mut PeekableTokens<'a>,
        typ: TokenType,
        message: &'static str,
    ) -> &'a Token {
        let maybe_token = Parser::is_match(tokens, &[typ]);

        match maybe_token {
            Some(_) => maybe_token.unwrap(),
            None => panic!(message),
        }
    }
}

// Member Methods
impl<'a> Parser<'a> {
    pub fn parse(&mut self) -> Vec<Stmt> {
        let mut statements = Vec::new();
        while let Some(_) = self.tokens.peek() {
            statements.push(Parser::declaration(&mut self.tokens));
        }
        statements
    }
}

#[cfg(test)]
mod tests {
    use super::Parser;
    use crate::ast::expr::Expr;
    use crate::ast::stmt::Stmt;
    use crate::ast::token::{Token, TokenType};

    #[test]
    fn simple_addition() {
        let line = 1;
        let tokens = vec![
            Token::new(TokenType::Number(1.0), String::from("1"), line),
            Token::new(TokenType::Plus, String::from("+"), line),
            Token::new(TokenType::Number(2.0), String::from("2"), line),
            Token::new(TokenType::Semicolon, String::from(";"), line),
        ];
        let mut parser = Parser::new(&tokens);
        let mut statements = Vec::new();
        let expected = Stmt::Expression(Expr::Binary(
            Box::new(Expr::Literal(Token::new(
                TokenType::Number(1.0),
                String::from("1"),
                line,
            ))),
            Token::new(TokenType::Plus, String::from("+"), line),
            Box::new(Expr::Literal(Token::new(
                TokenType::Number(2.0),
                String::from("2"),
                line,
            ))),
        ));

        statements.push(expected);

        assert_eq!(statements, parser.parse());
    }

    #[test]
    fn simple_grouping() {
        let line = 1;
        let tokens = vec![
            Token::new(TokenType::LeftParen, String::from("("), line),
            Token::new(TokenType::Number(1.0), String::from("1"), line),
            Token::new(TokenType::Plus, String::from("+"), line),
            Token::new(TokenType::Number(2.0), String::from("2"), line),
            Token::new(TokenType::RightParen, String::from(")"), line),
            Token::new(TokenType::Semicolon, String::from(";"), line),
        ];
        let mut parser = Parser::new(&tokens);
        let mut statements = Vec::new();
        let expected = Stmt::Expression(Expr::Grouping(Box::new(Expr::Binary(
            Box::new(Expr::Literal(Token::new(
                TokenType::Number(1.0),
                String::from("1"),
                line,
            ))),
            Token::new(TokenType::Plus, String::from("+"), line),
            Box::new(Expr::Literal(Token::new(
                TokenType::Number(2.0),
                String::from("2"),
                line,
            ))),
        ))));

        statements.push(expected);

        assert_eq!(statements, parser.parse());
    }

    #[test]
    #[should_panic]
    fn invalid_token_error() {
        let line = 1;
        let tokens = vec![
            Token::new(TokenType::LeftParen, String::from("("), line),
            Token::new(TokenType::Number(1.0), String::from("1"), line),
            Token::new(TokenType::Plus, String::from("+"), line),
            Token::new(TokenType::Number(2.0), String::from("2"), line),
        ];
        let mut parser = Parser::new(&tokens);
        parser.parse();
    }
}
