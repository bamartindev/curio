pub struct Html {
    source_path: String,
}

impl Html {
    pub fn new(source_path: String) -> Self {
        Self { source_path }
    }
}

impl Html {
    // TODO: This is ugly - can I save this template in a file and do some interop?
    pub fn generate_html(&self) -> String {
        format!(
            "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\">
    <title>{0}</title>
</head>
<body>
    <script type=\"text/javascript\" src=\"./{0}\"></script>
</body>
</html>",
            self.source_path
        )
    }
}
