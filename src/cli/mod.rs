mod html;

use std::env;
use std::fs;
use std::path::Path;

use webbrowser;

use crate::lexer::Lexer;
use crate::parser::Parser;
use crate::visitors::print::Print;
use html::Html;

const LANGUAGE_NAME: &str = "curio";
const JS_OUTPUT_DIR: &str = "build/out/src";
const JS_DIR: &str = "src";
const HTML_OUTPUT_DIR: &str = "build/out";

struct Args<'a> {
    filename: &'a str,
    is_prod: bool,
}

// Associated methods
impl<'a> Args<'a> {
    fn parse(args: &'a [String]) -> Self {
        let is_prod = match args.len() {
            2 => "-p" == args[1], // Prod Mode - I want to add some syntax that will be stripped out if prod mode => true.
            _ => false,
        };

        Self {
            filename: &args[0],
            is_prod,
        }
    }
}

pub struct CLI {
    filename: Option<String>,
}

// Associated methods
impl CLI {
    pub fn new() -> Self {
        Self { filename: None }
    }

    fn print_usage() {
        println!("Usage: curio [file] [-p]");
    }

    fn write_to_file(output_path: &Path, contents: String, filename: &str) {
        fs::create_dir_all(JS_OUTPUT_DIR).expect("Unable to create directory for file output");
        fs::write(output_path, contents).expect("Unable to write js file");

        // Once file is written, generate the HTML shell and include the js file generated
        let js_file_path = format!("{}/{}.js", JS_DIR, filename);
        let html_string = Html::new(js_file_path).generate_html();
        let path_name = format!("{}/index.html", HTML_OUTPUT_DIR);
        let html_output_path = Path::new(&path_name);
        fs::write(html_output_path, html_string).expect("unable to write index.html file");
    }

    fn print_startup_information() {
        let version = env!("CARGO_PKG_VERSION");
        println!("{} v{}", LANGUAGE_NAME, version);
    }
}

// Member methods
impl CLI {
    pub fn run(&mut self) {
        CLI::print_startup_information();

        // Skip the first arg, as it is just the 'curio.exe' self referential name
        let cmd_input: Vec<String> = env::args().skip(1).collect();
        if cmd_input.is_empty() {
            CLI::print_usage();
            std::process::exit(65);
        }

        // Check if the request was to launch the html shell
        if cmd_input[0] == "run" {
            let current_dir = env::current_dir().expect("couldn't retrieve current directory");
            let file = format!("{}/{}/index.html", current_dir.display(), HTML_OUTPUT_DIR);
            webbrowser::open(&file).expect("Could not open html shell, have you compiled?");
        } else {
            let args = Args::parse(&cmd_input);
            self.run_file(&args.filename, args.is_prod);
        }
    }

    fn run_file(&mut self, src_file_path: &str, is_prod: bool) {
        let path = Path::new(src_file_path);
        let display = path.display();
        self.set_output_filename(&path);

        println!("opening file at {}", display);

        let contents = fs::read_to_string(path).expect("Unable to read input file");

        self.compile(&contents, is_prod);
    }

    fn set_output_filename(&mut self, filepath: &Path) {
        let filename = filepath.file_stem().unwrap();
        let filename = String::from(filename.to_str().unwrap());
        self.filename = Some(filename);
    }

    fn compile(&self, source: &str, is_prod: bool) {
        println!("Compiling in production mode: {}", is_prod);
        // TODO: Can this be compacted in some way? Lots of declarations.
        let mut lexer = Lexer::new(source);
        let tokens = lexer.lex();
        let mut parser = Parser::new(&tokens);
        let statements = parser.parse();

        let mut printer = Print::new();
        let result = printer.generate_string(&statements);

        let filename = self.filename.clone().unwrap();
        let path_name = format!("{}/{}.js", JS_OUTPUT_DIR, filename);
        let output_path = Path::new(&path_name);
        CLI::write_to_file(output_path, result, &filename);
    }
}
