#[macro_use]
extern crate lazy_static;

mod ast;
mod cli;
mod lexer;
mod parser;
mod visitors;

use cli::CLI;

fn main() {
    let mut cli = CLI::new();
    cli.run();
}
